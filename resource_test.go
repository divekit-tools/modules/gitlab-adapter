package gitlabadapter

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

// GitLabClientInterface defines the methods we need to mock
type GitLabClientInterface interface {
	CreateCommit(pid interface{}, opt *gitlab.CreateCommitOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Commit, *gitlab.Response, error)
}

func TestReadProjectFiles(t *testing.T) {
	// Create temporary test directory
	tempDir, err := os.MkdirTemp("", "test-project-*")
	assert.NoError(t, err)
	defer os.RemoveAll(tempDir)

	// Create test file structure
	files := map[string]string{
		"test.txt":          "Hello World",
		"subfolder/test.go": "package main",
		".hidden":           "hidden file",
		"_norepo_skip.txt":  "should be ignored",
		".git/config":       "git config",
		"binary.png":        "\x89PNG\x0D\x0A\x1A\x0A", // PNG signature
	}

	for path, content := range files {
		fullPath := filepath.Join(tempDir, path)
		err := os.MkdirAll(filepath.Dir(fullPath), 0755)
		assert.NoError(t, err)
		err = os.WriteFile(fullPath, []byte(content), 0644)
		assert.NoError(t, err)
	}

	// Test reading project files
	actions, err := readProjectFiles(tempDir)
	assert.NoError(t, err)

	// Should only include non-hidden, non-ignored files
	assert.Equal(t, 3, len(actions))

	// Verify expected files are included
	foundPaths := make(map[string]bool)
	for _, action := range actions {
		foundPaths[*action.FilePath] = true
	}
	assert.True(t, foundPaths["test.txt"])
	assert.True(t, foundPaths["subfolder/test.go"])
	assert.True(t, foundPaths["binary.png"])
}

func TestIsHidden(t *testing.T) {
	tests := []struct {
		filename string
		want     bool
	}{
		{".hidden", true},
		{"normal.txt", false},
		{".git", true},
		{"", false},
		{"test.txt", false},
	}

	for _, tt := range tests {
		t.Run(tt.filename, func(t *testing.T) {
			got := isHidden(tt.filename)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestShouldIgnorePath(t *testing.T) {
	tests := []struct {
		path string
		want bool
	}{
		{".git/config", true},
		{".git", true},
		{"test_norepo_file.txt", true},
		{"normal/path/file.txt", false},
		{"src/main.go", false},
	}

	for _, tt := range tests {
		t.Run(tt.path, func(t *testing.T) {
			got := shouldIgnorePath(tt.path)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestIsBinary(t *testing.T) {
	tests := []struct {
		name    string
		content []byte
		want    bool
	}{
		{
			name:    "text file",
			content: []byte("Hello World"),
			want:    false,
		},
		{
			name:    "empty file",
			content: []byte{},
			want:    false,
		},
		{
			name:    "PNG file",
			content: []byte{0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A},
			want:    true,
		},
		{
			name:    "JPEG file",
			content: []byte{0xFF, 0xD8, 0xFF},
			want:    true,
		},
		{
			name:    "file with null bytes",
			content: []byte{'H', 'e', 'l', 'l', 'o', 0x00, 'W', 'o', 'r', 'l', 'd'},
			want:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := isBinary(tt.content)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestCreateCommitAction(t *testing.T) {
	// Create temporary test directory
	tempDir, err := os.MkdirTemp("", "test-commit-*")
	assert.NoError(t, err)
	defer os.RemoveAll(tempDir)

	// Test text file
	textPath := filepath.Join(tempDir, "test.txt")
	err = os.WriteFile(textPath, []byte("Hello World"), 0644)
	assert.NoError(t, err)

	action, err := createCommitAction(tempDir, textPath)
	assert.NoError(t, err)
	assert.Equal(t, "test.txt", *action.FilePath)
	assert.Equal(t, "Hello World", *action.Content)
	assert.Nil(t, action.Encoding)

	// Test binary file
	binaryPath := filepath.Join(tempDir, "test.png")
	binaryContent := []byte{0x89, 0x50, 0x4E, 0x47}
	err = os.WriteFile(binaryPath, binaryContent, 0644)
	assert.NoError(t, err)

	action, err = createCommitAction(tempDir, binaryPath)
	assert.NoError(t, err)
	assert.Equal(t, "test.png", *action.FilePath)
	assert.Equal(t, "base64", *action.Encoding)
}

// MockGitLabClient implements the necessary methods for tests
type MockGitLabClient struct {
	CommitService struct {
		CreateCommitFunc func(pid interface{}, opt *gitlab.CreateCommitOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Commit, *gitlab.Response, error)
	}
}

// Implement the necessary method
func (m *MockGitLabClient) CreateCommit(pid interface{}, opt *gitlab.CreateCommitOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Commit, *gitlab.Response, error) {
	if m.CommitService.CreateCommitFunc != nil {
		return m.CommitService.CreateCommitFunc(pid, opt, options...)
	}
	return nil, nil, fmt.Errorf("CreateCommit not implemented")
}
