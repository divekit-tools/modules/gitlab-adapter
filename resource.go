package gitlabadapter

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/apex/log"
	"github.com/schollz/progressbar/v3"
	"github.com/xanzy/go-gitlab"
)

var (
	ignorePatterns = []string{
		".git/*",    // Ignoriert alle Dateien im .git Verzeichnis
		".git",      // Ignoriert das .git Verzeichnis selbst
		"*_norepo*", // Ignoriert alles mit _norepo
	}
)

// UploadLocalProject uploads a local project directory to one or multiple GitLab repositories
func (g *GitLabType) UploadLocalProject(localPath string, repositories []*gitlab.Project, commitInfo CommitInfo) error {
	files, err := readProjectFiles(localPath)
	if err != nil {
		return fmt.Errorf("error reading project files: %w", err)
	}

	return g.uploadToRepositories(files, repositories, commitInfo)
}

func (g *GitLabType) uploadToRepositories(files []*gitlab.CommitActionOptions, repositories []*gitlab.Project, commitInfo CommitInfo) error {
	if len(files) == 0 {
		return fmt.Errorf("no files found to upload")
	}

	log.Debugf("Found %d files to upload", len(files))

	var errs []error
	for _, repo := range repositories {
		log.Debugf("Uploading to repository %s (ID: %d)", repo.Name, repo.ID)
		if err := g.createInitialCommit(repo, files, commitInfo); err != nil {
			log.Errorf("Failed to upload project to repository %s: %v", repo.Name, err)
			errs = append(errs, fmt.Errorf("repository %s: %w", repo.Name, err))
			continue
		}
		log.Debugf("Successfully uploaded project to repository %s", repo.Name)
	}

	if len(errs) > 0 {
		return fmt.Errorf("failed to upload to some repositories: %v", errs)
	}
	return nil
}

// readProjectFiles reads all files from a directory recursively
func readProjectFiles(rootPath string) ([]*gitlab.CommitActionOptions, error) {
	var actions []*gitlab.CommitActionOptions

	err := filepath.Walk(rootPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() || shouldIgnorePath(path) {
			if info.IsDir() && shouldIgnorePath(path) {
				return filepath.SkipDir
			}
			return nil
		}

		action, err := createCommitAction(rootPath, path)
		if err != nil {
			return err
		}
		actions = append(actions, action)
		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("error walking through project directory: %w", err)
	}

	return actions, nil
}

// createInitialCommit creates a single commit with all project files
func (g *GitLabType) createInitialCommit(repo *gitlab.Project, files []*gitlab.CommitActionOptions, commitInfo CommitInfo) error {
	if len(files) == 0 {
		return fmt.Errorf("no files to commit")
	}

	commitOpts := &gitlab.CreateCommitOptions{
		Branch:        gitlab.Ptr("main"),
		CommitMessage: gitlab.Ptr(commitInfo.Message),
		Actions:       files,
		AuthorName:    gitlab.Ptr(commitInfo.AuthorName),
		AuthorEmail:   gitlab.Ptr(commitInfo.AuthorEmail),
	}

	log.Debugf("Creating commit in repository %s with %d files", repo.Name, len(files))
	_, _, err := g.client.Commits.CreateCommit(repo.ID, commitOpts)
	if err != nil {
		return fmt.Errorf("error creating commit: %w", err)
	}

	return nil
}

// isHidden checks if a file is hidden (starts with a dot)
func isHidden(filename string) bool {
	return len(filename) > 0 && filename[0] == '.'
}

// shouldIgnorePath checks if a path matches any ignore patterns
func shouldIgnorePath(path string) bool {
	// Konvertiere den Pfad zu Unix-Style für konsistenten Vergleich
	path = filepath.ToSlash(path)

	// Extrahiere den Dateinamen für die Prüfung auf versteckte Dateien
	filename := filepath.Base(path)
	if isHidden(filename) {
		return true
	}

	for _, pattern := range ignorePatterns {
		// Konvertiere auch das Pattern zu Unix-Style
		pattern = filepath.ToSlash(pattern)

		// Verwende filepath.Match für den Vergleich
		matched, err := filepath.Match(pattern, path)
		if err == nil && matched {
			return true
		}

		// Prüfe auch, ob ein Teil des Pfades dem Pattern entspricht
		pathParts := strings.Split(path, "/")
		for _, part := range pathParts {
			matched, err := filepath.Match(pattern, part)
			if err == nil && matched {
				return true
			}
		}
	}
	return false
}

// isBinary detects if content is binary by checking common file signatures and null bytes
func isBinary(content []byte) bool {
	if len(content) == 0 {
		return false
	}

	// Common binary signatures
	sigatures := map[string][]byte{
		"PNG":  {0x89, 0x50, 0x4E, 0x47},
		"JPEG": {0xFF, 0xD8},
		"GIF":  {0x47, 0x49, 0x46},
	}

	for _, sig := range sigatures {
		if len(content) >= len(sig) && bytes.Equal(content[:len(sig)], sig) {
			return true
		}
	}

	// Check first 512 bytes for null bytes
	checkLength := min(len(content), 512)
	return bytes.IndexByte(content[:checkLength], 0) != -1
}

func createCommitAction(rootPath string, path string) (*gitlab.CommitActionOptions, error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error reading file %s: %w", path, err)
	}

	// Use the full path as is, just convert backslashes to forward slashes
	filePath := filepath.ToSlash(path)

	log.Debugf("Adding file to commit: %s", filePath)

	action := gitlab.FileCreate
	actionOpts := &gitlab.CommitActionOptions{
		Action:   &action,
		FilePath: gitlab.Ptr(filePath),
	}

	if isBinary(content) {
		actionOpts.Content = gitlab.Ptr(base64.StdEncoding.EncodeToString(content))
		actionOpts.Encoding = gitlab.Ptr("base64")
	} else {
		actionOpts.Content = gitlab.Ptr(string(content))
	}

	return actionOpts, nil
}

// FileStatus represents the status of a file in remote repositories
type FileStatus struct {
	Path     string
	Exists   bool
	Projects []int
}

// ValidateFiles checks if the given files exist in the specified GitLab projects
func (g *GitLabType) ValidateFiles(projectIDs []int, files []string, checkAllProjects bool) (map[string]*FileStatus, error) {
	if len(projectIDs) == 0 {
		return nil, fmt.Errorf("no project IDs provided")
	}

	log.Debug("ValidateFiles called with:")
	log.Debugf("  ProjectIDs: %v", projectIDs)
	log.Debugf("  Files to check: %v", files)
	log.Debugf("  Check all projects: %v", checkAllProjects)

	fileStatuses := make(map[string]*FileStatus)

	// Initialize status for each file with normalized paths
	for _, file := range files {
		// Convert backslashes and remove leading ./ or .\
		normalizedPath := filepath.ToSlash(file)
		normalizedPath = strings.TrimPrefix(normalizedPath, "./")
		fileStatuses[normalizedPath] = &FileStatus{
			Path:     normalizedPath,
			Exists:   false,
			Projects: make([]int, 0),
		}
	}

	// If we don't need to check all projects, only check the first one
	projectsToCheck := projectIDs
	if !checkAllProjects {
		projectsToCheck = projectIDs[:1]
		log.Debugf("Checking only first project: %d", projectsToCheck[0])
	}

	// Check each project
	for _, projectID := range projectsToCheck {
		log.Debugf("Checking project %d", projectID)
		for filePath := range fileStatuses {
			normalizedPath := filepath.ToSlash(filePath) // Convert backslashes to forward slashes
			normalizedPath = strings.TrimPrefix(normalizedPath, "./")
			log.Debugf("  Checking file: %s (normalized: %s)", filePath, normalizedPath)

			// Check if file exists in the project using normalized path
			file, resp, err := g.client.RepositoryFiles.GetFile(
				projectID,
				normalizedPath,
				&gitlab.GetFileOptions{Ref: gitlab.Ptr("main")},
			)

			if resp != nil {
				log.Debugf("    Response status: %d", resp.StatusCode)
			}

			if err != nil {
				log.Debugf("    Error: %v", err)
			} else {
				log.Debugf("    File found: %v", file != nil)
			}

			if err == nil {
				fileStatuses[filePath].Exists = true
				if checkAllProjects {
					fileStatuses[filePath].Projects = append(fileStatuses[filePath].Projects, projectID)
				} else {
					fileStatuses[filePath].Projects = projectIDs
				}
				log.Debugf("    Marked as existing: %s", filePath)
			}
		}
	}

	log.Debug("Final file statuses:")
	for file, status := range fileStatuses {
		log.Debugf("  %s: exists=%v, projects=%v", file, status.Exists, status.Projects)
	}

	return fileStatuses, nil
}

// UploadFiles applies the planned changes to all specified remote projects
func (g *GitLabType) UploadFiles(projectIDs []int, remoteFiles map[string]*FileStatus, localFiles map[string]*FileStatus) error {
	if len(projectIDs) == 0 {
		return fmt.Errorf("no project IDs provided")
	}

	// Create normalized map for local files
	normalizedLocalFiles := make(map[string]*FileStatus)
	for file, status := range localFiles {
		normalizedPath := filepath.ToSlash(file)
		normalizedPath = strings.TrimPrefix(normalizedPath, "./")
		normalizedLocalFiles[normalizedPath] = status
	}

	// Prepare actions for each file
	var actions []*gitlab.CommitActionOptions
	for file := range remoteFiles {
		existsRemote := remoteFiles[file].Exists
		existsLocal := normalizedLocalFiles[file].Exists

		if !existsRemote && existsLocal || existsRemote && existsLocal {
			// Create or update file
			action, err := createCommitAction(filepath.Dir(file), file)
			if err != nil {
				return fmt.Errorf("error preparing file action for %s: %w", file, err)
			}
			if existsRemote {
				*action.Action = gitlab.FileUpdate
			}
			actions = append(actions, action)
		} else if existsRemote && !existsLocal {
			// Delete file
			action := gitlab.FileDelete
			actions = append(actions, &gitlab.CommitActionOptions{
				Action:   &action,
				FilePath: gitlab.Ptr(file),
			})
		}
	}

	// Apply changes to each project
	log.Info("\nApplying changes to projects...")
	bar := progressbar.Default(int64(len(projectIDs)))

	for _, projectID := range projectIDs {
		commitOpts := &gitlab.CreateCommitOptions{
			Branch:        gitlab.Ptr("main"),
			CommitMessage: gitlab.Ptr("Update files via patch command"),
			Actions:       actions,
			AuthorName:    gitlab.Ptr("Divekit"),
			AuthorEmail:   gitlab.Ptr("divekit@git.nrw"),
		}

		log.Debugf("Applying changes to project %d", projectID)
		_, _, err := g.client.Commits.CreateCommit(projectID, commitOpts)
		if err != nil {
			return fmt.Errorf("error applying changes to project %d: %w", projectID, err)
		}
		bar.Add(1)
	}

	return nil
}
