package gitlabadapter

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func setupTestServer() (*httptest.Server, *GitLabType) {
	// Create a test server
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/api/v4/users":
			username := r.URL.Query().Get("username")
			if username == "existinguser" {
				fmt.Fprint(w, `[{"id":1, "username":"existinguser"}]`)
			} else if username == "erroruser" {
				w.WriteHeader(http.StatusInternalServerError)
			} else {
				fmt.Fprint(w, `[]`)
			}
		case "/api/v4/projects/1/members/1":
			if r.Method == http.MethodGet {
				fmt.Fprint(w, `{"id":1, "access_level":30}`)
			}
		case "/api/v4/projects/1/members":
			if r.Method == http.MethodPost {
				fmt.Fprint(w, `{"id":1}`)
			}
		}
	}))

	// Create a GitLab client using the test server URL
	client, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
	return server, &GitLabType{client: client}
}

func TestUserExists(t *testing.T) {
	server, gitlab := setupTestServer()
	defer server.Close()

	tests := []struct {
		name           string
		username       string
		expectExists   bool
		expectError    bool
		expectedUserID int
	}{
		{
			name:           "Existing user",
			username:       "existinguser",
			expectExists:   true,
			expectError:    false,
			expectedUserID: 1,
		},
		{
			name:         "Non-existing user",
			username:     "nonexistinguser",
			expectExists: false,
			expectError:  false,
		},
		{
			name:         "Error case",
			username:     "erroruser",
			expectExists: false,
			expectError:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			user, exists, err := gitlab.UserExists(tt.username)

			if tt.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectExists, exists)
				if tt.expectExists {
					assert.Equal(t, tt.expectedUserID, user.ID)
				}
			}
		})
	}
}

func TestAssignMemberToProject(t *testing.T) {
	server, gitlab := setupTestServer()
	defer server.Close()

	tests := []struct {
		name        string
		projectID   int
		username    string
		accessLevel int
		expectError bool
	}{
		{
			name:        "Assign existing member",
			projectID:   1,
			username:    "existinguser",
			accessLevel: 30,
			expectError: false,
		},
		{
			name:        "Assign non-existing member",
			projectID:   1,
			username:    "nonexistinguser",
			accessLevel: 30,
			expectError: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := gitlab.AssignMemberToProject(tt.projectID, tt.username, tt.accessLevel)
			if tt.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestCheckMembersAvailability(t *testing.T) {
	server, gitlab := setupTestServer()
	defer server.Close()

	tests := []struct {
		name              string
		members           map[string][]string
		expectAvailable   []string
		expectUnavailable []string
	}{
		{
			name: "Mixed users",
			members: map[string][]string{
				"uuid1": {"existinguser", "nonexistinguser"},
				"uuid2": {"existinguser"},
			},
			expectAvailable:   []string{"existinguser"},
			expectUnavailable: []string{"nonexistinguser"},
		},
		{
			name: "All existing users",
			members: map[string][]string{
				"uuid1": {"existinguser"},
			},
			expectAvailable:   []string{"existinguser"},
			expectUnavailable: []string{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			available, unavailable := gitlab.CheckMembersAvailability(tt.members)

			assert.ElementsMatch(t, tt.expectAvailable, available)
			assert.ElementsMatch(t, tt.expectUnavailable, unavailable)
		})
	}
}

func TestMapUsersToUUIDs(t *testing.T) {
	gitlab := &GitLabType{}

	tests := []struct {
		name           string
		members        map[string][]string
		availableUsers []string
		expected       map[string]string
	}{
		{
			name: "Simple mapping",
			members: map[string][]string{
				"uuid1": {"user1", "user2"},
				"uuid2": {"user3"},
			},
			availableUsers: []string{"user1", "user3"},
			expected: map[string]string{
				"user1": "uuid1",
				"user3": "uuid2",
			},
		},
		{
			name: "No available users",
			members: map[string][]string{
				"uuid1": {"user1"},
			},
			availableUsers: []string{},
			expected:       map[string]string{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := gitlab.mapUsersToUUIDs(tt.members, tt.availableUsers)
			assert.Equal(t, tt.expected, result)
		})
	}
}
