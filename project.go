package gitlabadapter

import (
	"fmt"
	"strings"

	"github.com/apex/log"
	"github.com/xanzy/go-gitlab"
)

func (g *GitLabType) CreateProject(name string, groupID int) (*gitlab.Project, error) {
	// Validate group access
	group, _, err := g.client.Groups.GetGroup(groupID, nil)
	if err != nil {
		return nil, NewGitLabError("CreateProject", fmt.Errorf("cannot access group %d: %w", groupID, err))
	}
	log.Debugf("Successfully found group: %s (ID: %d)", group.Name, group.ID)

	// Check for existing project
	project, err := g.findExistingProject(name, groupID)
	if err != nil {
		return nil, err
	}
	if project != nil {
		return project, nil
	}

	// Create new project
	return g.createNewProject(name, groupID)
}

// Helper functions
func (g *GitLabType) findExistingProject(name string, groupID int) (*gitlab.Project, error) {
	projects, _, err := g.client.Groups.ListGroupProjects(groupID, &gitlab.ListGroupProjectsOptions{
		Search: gitlab.Ptr(name),
	})
	if err != nil {
		return nil, NewGitLabError("findExistingProject", err)
	}

	for _, p := range projects {
		if p.Name == name && p.Namespace.ID == groupID {
			log.Debugf("Project %s already exists in group %d", name, groupID)
			return p, nil
		}
	}

	return nil, nil
}

func (g *GitLabType) createNewProject(name string, groupID int) (*gitlab.Project, error) {
	opts := &gitlab.CreateProjectOptions{
		Name:                 gitlab.Ptr(name),
		NamespaceID:          gitlab.Ptr(groupID),
		InitializeWithReadme: gitlab.Ptr(false),
	}

	project, resp, err := g.client.Projects.CreateProject(opts)
	if err != nil {
		if resp != nil && resp.StatusCode == 400 {
			// Try to find existing project as fallback
			existingProject, _, findErr := g.client.Projects.ListUserProjects(nil, &gitlab.ListProjectsOptions{
				Search: gitlab.Ptr(name),
				Owned:  gitlab.Ptr(true),
			}, nil)
			if findErr == nil && len(existingProject) > 0 {
				for _, p := range existingProject {
					if p.Name == name && p.Namespace.ID == groupID {
						return p, nil
					}
				}
			}
		}
		return nil, NewGitLabError("createNewProject", fmt.Errorf("failed to create project %s: %w", name, err))
	}

	return project, nil
}

func (g *GitLabType) AssignMemberToProject(projectID int, username string, accessLevel int) error {
	users, _, err := g.client.Users.ListUsers(&gitlab.ListUsersOptions{
		Username: gitlab.Ptr(username),
	})
	if err != nil {
		return NewGitLabError("AssignMemberToProject", err)
	}
	if len(users) == 0 {
		return fmt.Errorf("user %s not found", username)
	}

	userID := users[0].ID
	opts := &gitlab.AddProjectMemberOptions{
		UserID:      gitlab.Ptr(userID),
		AccessLevel: gitlab.Ptr(gitlab.AccessLevelValue(accessLevel)),
	}

	member, _, err := g.client.ProjectMembers.GetProjectMember(projectID, userID)
	if err == nil && member != nil {
		log.Debugf("User %s already has access level %d on project %d", username, member.AccessLevel, projectID)
		return nil
	}

	_, resp, err := g.client.ProjectMembers.AddProjectMember(projectID, opts)
	if err != nil && resp != nil {
		if resp.StatusCode == 400 || resp.StatusCode == 409 {
			log.Debugf("Member operation for %s on project %d resulted in status %d: %v",
				username, projectID, resp.StatusCode, err)
			return nil
		}
		return err
	}
	return nil
}

// AssignMembersToProjects weist Mitglieder den Projects zu
func (g *GitLabType) AssignMembersToProjects(projects []*gitlab.Project, members []string) error {
	for i, project := range projects {
		if i >= len(members) {
			break
		}

		if err := g.AssignMemberToProject(project.ID, members[i], int(gitlab.MaintainerPermissions)); err != nil {
			return NewGitLabError("AssignMembersToProjects",
				fmt.Errorf("failed to assign member %s to project %s: %w", members[i], project.Name, err))
		}
	}
	return nil
}

// CreateDistributionProjects erstellt Projects für die Distribution und gibt die Projekte sowie Mappings zurück
func (g *GitLabType) CreateDistributionProjects(members map[string][]string, projectNamePattern string, groupID int, availableUsers []string) ([]*gitlab.Project, *Remotes, error) {
	var projects []*gitlab.Project
	var errors []error
	mappings := &Remotes{
		Remotes: make([]RemoteMapping, 0),
	}

	userUUIDs := g.mapUsersToUUIDs(members, availableUsers)

	for username, uuid := range userUUIDs {
		projectName := strings.ReplaceAll(projectNamePattern, "{{uuid}}", uuid)
		project, err := g.CreateProject(projectName, groupID)
		if err != nil {
			log.Debugf("Problem with project %s: %v", projectName, err)
			errors = append(errors, NewGitLabError("CreateDistributionProjects",
				fmt.Errorf("failed to create project %s: %w", projectName, err)))
			continue
		}

		if project != nil {
			if project.CreatedAt == nil {
				log.Debugf("Project %s already exists for user %s", projectName, username)
			}
			projects = append(projects, project)

			// Add mapping
			mappings.Remotes = append(mappings.Remotes, RemoteMapping{
				UUID:      uuid,
				ProjectID: project.ID,
			})
		}
	}

	if len(errors) > 0 && len(projects) > 0 {
		for _, err := range errors {
			log.Debugf("Project creation error: %v", err)
		}
		return projects, mappings, nil
	} else if len(errors) > 0 {
		return nil, nil, errors[0]
	}

	return projects, mappings, nil
}
