package gitlabadapter

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func TestCheckTokenPermissions(t *testing.T) {
	tests := []struct {
		name           string
		mockResponse   *gitlab.PersonalAccessToken
		mockStatusCode int
		want           *TokenPermissions
		wantErr        bool
	}{
		{
			name: "Valid token with all permissions",
			mockResponse: &gitlab.PersonalAccessToken{
				Scopes: []string{"api", "read_user", "read_api"},
			},
			mockStatusCode: http.StatusOK,
			want: &TokenPermissions{
				TokenIsValid:   true,
				HasValidScopes: true,
				Scopes:         []string{"api", "read_user", "read_api"},
			},
			wantErr: false,
		},
		{
			name:           "Invalid token",
			mockResponse:   nil,
			mockStatusCode: http.StatusUnauthorized,
			want:           nil,
			wantErr:        true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create test server
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(tt.mockStatusCode)
				if tt.mockResponse != nil {
					json.NewEncoder(w).Encode(tt.mockResponse)
				}
			}))
			defer ts.Close()

			// Create client with test server
			client, err := gitlab.NewClient("test-token", gitlab.WithBaseURL(ts.URL))
			assert.NoError(t, err)

			gitlab := &GitLabType{client: client}

			// Test
			got, err := gitlab.CheckTokenPermissions()

			if tt.wantErr {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.want, got)
		})
	}
}
