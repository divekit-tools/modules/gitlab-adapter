package gitlabadapter

import "github.com/apex/log"

// TokenPermissions repräsentiert die Berechtigungsprüfungsergebnisse
type TokenPermissions struct {
	HasValidScopes bool
	TokenIsValid   bool
	Scopes         []string
}

// CheckTokenPermissions überprüft die Berechtigungen des verwendeten Tokens
func (g *GitLabType) CheckTokenPermissions() (*TokenPermissions, error) {
	log.Debug("gitlabadapter.CheckTokenPermissions")
	permissions := &TokenPermissions{}

	// Try to get user info instead of token info as it's more reliable
	user, resp, err := g.client.Users.CurrentUser()
	if err != nil {
		if resp != nil && resp.StatusCode == 401 {
			permissions.TokenIsValid = false
			return permissions, nil
		}
		return nil, NewGitLabError("CheckTokenPermissions", err)
	}

	permissions.TokenIsValid = (resp.StatusCode == 200)
	log.Debugf("Token is valid: %v", permissions.TokenIsValid)
	log.Debugf("User: %v", user)
	log.Debugf("Response: %v", resp)

	// If we can access the API, we have sufficient permissions
	if user != nil {
		permissions.HasValidScopes = true
		permissions.Scopes = []string{"api"} // Assume api scope if we can access the API
	}

	return permissions, nil
}
