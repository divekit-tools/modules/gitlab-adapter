package gitlabadapter

import (
	"fmt"
	"slices"
	"sync"

	"github.com/apex/log"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/git.nrw/divekit/modules/config-management"
)

func (g *GitLabType) UserExists(username string) (*gitlab.User, bool, error) {
	opts := &gitlab.ListUsersOptions{
		Username: &username,
		Search:   &username,
		ListOptions: gitlab.ListOptions{
			PerPage: 1,
		},
	}

	users, _, err := g.client.Users.ListUsers(opts)
	if err != nil {
		return nil, false, NewGitLabError("UserExists", err)
	}
	if len(users) == 0 {
		return nil, false, nil
	}
	return users[0], true, nil
}

// CheckMembersAvailability prüft welche Mitglieder in GitLab verfügbar sind
func (g *GitLabType) CheckMembersAvailability(members map[string][]string) ([]string, []string) {
	var available, unavailable []string
	checked := make(map[string]bool)
	var mutex sync.Mutex
	var wg sync.WaitGroup

	// Sammle alle einzigartigen Benutzernamen
	usernames := make([]string, 0)
	for _, names := range members {
		for _, username := range names {
			if _, exists := checked[username]; !exists {
				checked[username] = true
				usernames = append(usernames, username)
			}
		}
	}

	// Erstelle einen Worker-Pool
	workers := 5
	usernameChan := make(chan string, len(usernames))
	for _, username := range usernames {
		usernameChan <- username
	}
	close(usernameChan)

	// Starte Worker
	for i := 0; i < workers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for username := range usernameChan {
				_, exists, err := g.UserExists(username)
				mutex.Lock()
				if err != nil {
					log.Warnf("Error checking user %s: %v", username, err)
					unavailable = append(unavailable, username)
				} else if exists {
					available = append(available, username)
				} else {
					unavailable = append(unavailable, username)
				}
				mutex.Unlock()
			}
		}()
	}

	wg.Wait()
	return available, unavailable
}

// Helper functions
func (g *GitLabType) mapUsersToUUIDs(members map[string][]string, availableUsers []string) map[string]string {
	userUUIDs := make(map[string]string)
	for uuid, usernames := range members {
		for _, username := range usernames {
			if slices.Contains(availableUsers, username) {
				userUUIDs[username] = uuid
				break
			}
		}
	}
	return userUUIDs
}

// ValidateMembers prüft die Verfügbarkeit der Mitglieder und gibt deren Status zurück
func (g *GitLabType) ValidateMembers(members *config.MembersConfig) (*config.MemberStatus, map[string][]string, error) {
	log.Debug("Checking member availability...")
	groupsMap := ConvertMembersToMap(members)
	available, unavailable := g.CheckMembersAvailability(groupsMap)

	if len(available) == 0 {
		return nil, nil, fmt.Errorf("no available users found to create projects for")
	}

	if len(unavailable) > 0 {
		log.Warn(fmt.Sprintf("Some members were not found: %v", unavailable))
	}

	return &config.MemberStatus{
		Available:   available,
		Unavailable: unavailable,
	}, groupsMap, nil
}

func ConvertMembersToMap(members *config.MembersConfig) map[string][]string {
	groupsMap := make(map[string][]string)
	for _, group := range members.Groups {
		groupsMap[group.UUID] = group.Members
	}
	return groupsMap
}
