package gitlabadapter

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
)

// NewGitLabClient creates a new GitLab client with the given token and baseURL
func NewGitLabClient(token string, baseURL string) (*GitLabType, error) {
	if err := validateClientParams(token, baseURL); err != nil {
		return nil, err
	}

	client, err := gitlab.NewClient(token, gitlab.WithBaseURL(baseURL))
	if err != nil {
		return nil, NewGitLabError("NewGitLabClient", fmt.Errorf("failed to create GitLab client: %w", err))
	}

	gitlabType := &GitLabType{
		client: client,
	}

	// Check token permissions
	permissions, err := gitlabType.CheckTokenPermissions()
	if err != nil {
		return nil, NewGitLabError("NewGitLabClient", fmt.Errorf("failed to check token permissions: %w", err))
	}

	if !permissions.TokenIsValid {
		return nil, NewGitLabError("NewGitLabClient", fmt.Errorf("invalid token: authentication failed"))
	}

	if !permissions.HasValidScopes {
		return nil, NewGitLabError("NewGitLabClient", fmt.Errorf("invalid token: insufficient permissions"))
	}

	return gitlabType, nil
}

// Helper function
func validateClientParams(token, baseURL string) error {
	if token == "" {
		return NewGitLabError("validateClientParams", fmt.Errorf("GitLab token is required"))
	}

	if baseURL == "" {
		return NewGitLabError("validateClientParams", fmt.Errorf("GitLab base URL is required"))
	}

	return nil
}
