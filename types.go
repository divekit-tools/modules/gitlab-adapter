package gitlabadapter

import "github.com/xanzy/go-gitlab"

// Repository related interfaces
type RepositoryManager interface {
	CreateProject(name string, groupID int) (*gitlab.Project, error)
	UploadLocalProject(localPath string, repositories []*gitlab.Project, commitInfo CommitInfo) error
	AssignMemberToRepository(projectID int, username string, accessLevel int) error
}

// User related interfaces
type UserManager interface {
	UserExists(username string) (*gitlab.User, bool, error)
	AssignMemberToRepository(projectID int, username string, accessLevel int) error
}

// Distribution related interfaces
type DistributionManager interface {
	CreateDistributionRepositories(members map[string][]string, repoNamePattern string, groupID int, availableUsers []string) ([]*gitlab.Project, error)
	CheckMembersAvailability(members map[string][]string) ([]string, []string)
	AssignMembersToRepositories(repositories []*gitlab.Project, members []string) error
}

type GitLabClient interface {
	RepositoryManager
	UserManager
	DistributionManager
}

// Common types
type RepositoryConfigContentType struct {
	Remote struct {
		TestRepositoryTargetGroupId int
	}
}

type GroupData struct {
	RepositoryName string
	Records        []map[string]string
}

type GitLabType struct {
	client *gitlab.Client
}

// CommitInfo contains information for creating a commit
type CommitInfo struct {
	AuthorName  string
	AuthorEmail string
	Message     string
}

// Custom error types
type GitLabError struct {
	Op  string
	Err error
}

type DistributionResult struct {
	Repositories     []*gitlab.Project
	TestRepositories []*gitlab.Project
	AvailableUsers   []string
	UnavailableUsers []string
}

type RemoteMapping struct {
	UUID      string `json:"uuid"`
	ProjectID int    `json:"project_id"`
}

type Remotes struct {
	Remotes []RemoteMapping `json:"remotes"`
}

func (e *GitLabError) Error() string {
	return e.Op + ": " + e.Err.Error()
}

func NewGitLabError(op string, err error) *GitLabError {
	return &GitLabError{Op: op, Err: err}
}
