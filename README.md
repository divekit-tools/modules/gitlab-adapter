
# Divekit Gitlab Adapter

## How this was created

```bash
go mod init gitlab.com/git.nrw/divekit/modules/gitlab-adapter
```

create `main.go` and add code to it.

install dependencies

```bash
go get github.com/xanzy/go-gitlab
go get github.com/apex/log
```

```bash
git init
git remote add origin https://gitlab.com/git.nrw/divekit/modules/gitlab-adapter.git
```

## How to use

### Initialize the Client

To use the GitLab adapter, first initialize the client with your GitLab token and base URL:

```go
package main

import (
	"fmt"
	"gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

func main() {
	token := "your_gitlab_token"
	baseURL := "https://gitlab.com"

	client, err := gitlabapi.NewGitLabClient(token, baseURL)
	if err != nil {
		fmt.Println("Error creating GitLab client:", err)
		return
	}

	// Example usage
	username := "example_user"
	user, exists, err := client.UserExists(username)
	if err != nil {
		fmt.Println("Error checking if user exists:", err)
		return
	} 
	if exists {
		fmt.Printf("User %s exists: %+v\n", username, user)
		
		// Example for repository access
		projectID := 123
		accessLevel := 30 // Developer access
		err = client.AssignMemberToRepository(projectID, username, accessLevel)
		if err != nil {
			fmt.Println("Error assigning member to repository:", err)
			return
		}
	} else {
		fmt.Printf("User %s does not exist\n", username)
	}
}
```

### Available Features

The GitLab Adapter provides several key features through its interfaces:

1. **Repository Management** (`RepositoryManager`)
   - Create new projects in GitLab groups
   - Upload local projects to GitLab repositories
   - Manage repository access

2. **User Management** (`UserManager`)
   - Check user existence
   - Manage repository access for users

3. **Distribution Management** (`DistributionManager`)
   - Create and distribute repositories to groups
   - Check member availability
   - Assign repository access

### Repository Management Examples

To create and manage repositories:

```go
package main

import (
	"fmt"
	"gitlab.com/git.nrw/divekit/modules/gitlab-adapter"
)

func main() {
	token := "your_gitlab_token"
	baseURL := "https://gitlab.com"

	client, err := gitlabapi.NewGitLabClient(token, baseURL)
	if err != nil {
		fmt.Println("Error creating GitLab client:", err)
		return
	}

	// Create a new project in a group
	projectName := "my-new-project"
	groupID := 12345
	project, err := client.CreateProject(projectName, groupID)
	if err != nil {
		fmt.Println("Error creating project:", err)
		return
	}

	// Upload a local project to GitLab
	localPath := "./my-local-project"
	commitInfo := gitlabapi.CommitInfo{
		AuthorName:  "John Doe",
		AuthorEmail: "john@example.com",
		Message:     "Initial commit",
	}
	err = client.UploadLocalProject(localPath, []*gitlab.Project{project}, commitInfo)
	if err != nil {
		fmt.Println("Error uploading project:", err)
		return
	}

	// Assign a member to the repository
	username := "developer1"
	accessLevel := gitlab.DeveloperPermissions
	err = client.AssignMemberToProject(project.ID, username, int(accessLevel))
	if err != nil {
		fmt.Println("Error assigning member:", err)
		return
	}
}
```

### Distribution Management Examples

```go
	// Define group members
	members := map[string][]string{
		"group1": {"user1", "user2"},
		"group2": {"user3", "user4"},
	}
	
	// Check member availability
	availableUsers, unavailableUsers := client.CheckMembersAvailability(members)
	fmt.Printf("Available users: %v\n", availableUsers)
	fmt.Printf("Unavailable users: %v\n", unavailableUsers)
	
	// Create distributed repositories
	repoNamePattern := "exercise-{{uuid}}"  // UUID will be replaced with group identifier
	groupID := 12345
	result, err := client.CreateDistributionRepositories(members, repoNamePattern, groupID, availableUsers)
	if err != nil {
		fmt.Println("Error creating distribution repositories:", err)
		return
	}

	// Assign members to their repositories
	err = client.AssignMembersToProjects(result.Repositories, availableUsers)
	if err != nil {
		fmt.Println("Error assigning members:", err)
		return
	}
```

This example demonstrates the main features of the GitLab adapter. Adjust the parameters according to your specific use case:

- `token`: Your GitLab personal access token
- `baseURL`: Your GitLab instance URL
- `groupID`: The ID of your target GitLab group
- `members`: Map of group names to member usernames
- `repoNamePattern`: Pattern for repository names (supports {{uuid}} placeholder)

### Error Handling

The adapter uses custom error types for better error handling:

```go
if err != nil {
	if gitlabErr, ok := err.(*gitlabapi.GitLabError); ok {
		fmt.Printf("GitLab operation '%s' failed: %v\n", gitlabErr.Op, gitlabErr.Err)
	} else {
		fmt.Printf("Unknown error: %v\n", err)
	}
}
```

### Configuration

The adapter supports various configuration options through the GitLab client:

```go
import "github.com/xanzy/go-gitlab"

// Custom client configuration
client, err := gitlabapi.NewGitLabClient(
	token,
	gitlab.WithBaseURL("https://gitlab.example.com"),
)
```

## Development

### Testing

The adapter includes comprehensive tests. To run them:

```bash
go test ./... -v
```

### Contributing

1. Fork the repository
2. Create your feature branch (`git checkout -b feature/amazing-feature`)
3. Commit your changes (`git commit -m 'Add some amazing feature'`)
4. Push to the branch (`git push origin feature/amazing-feature`)
5. Open a Pull Request

## Versioning
To version your module, you can create tags in your Git repository. This helps in managing different versions of your module.

```bash
git tag v1.1.0
git push origin v1.1.0
```
### Updating the module in Divekit
To update the module in Divekit, you need to update the version in the `go.mod` file of the module that uses this module.

```go
module gitlab-adapter

go 1.22.1

require (
    gitlab.com/git.nrw/divekit/modules/gitlab-adapter v1.1.0
)
```

or

```bash
go get gitlab.com/git.nrw/divekit/modules/gitlab-adapter@v1.1.0
```

you might need to run `go mod tidy` to update the dependencies.
